#ifndef MOTORDRIVER
#define MOTORDRIVER
#include <stdint.h>

void initMotorsDriver();

// if value is negative reverse the direction
// value will be between -100 to 100 (floating point)

void setMotorSpeeds(float speedA, float speedB);

#endif // MOTORDRIVER

