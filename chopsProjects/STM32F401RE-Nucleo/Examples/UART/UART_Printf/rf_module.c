#include "rf_module.h"

#define _NRF24L01P_TX_FIFO_COUNT   3
#define _NRF24L01P_RX_FIFO_COUNT   3

#define _NRF24L01P_TX_FIFO_SIZE   32
#define _NRF24L01P_RX_FIFO_SIZE   32

#define _NRF24L01P_SPI_MAX_DATA_RATE     10000000

#define _NRF24L01P_SPI_CMD_RD_REG            0x00
#define _NRF24L01P_SPI_CMD_WR_REG            0x20
#define _NRF24L01P_SPI_CMD_RD_RX_PAYLOAD     0x61
#define _NRF24L01P_SPI_CMD_WR_TX_PAYLOAD     0xa0
#define _NRF24L01P_SPI_CMD_FLUSH_TX          0xe1
#define _NRF24L01P_SPI_CMD_FLUSH_RX          0xe2
#define _NRF24L01P_SPI_CMD_REUSE_TX_PL       0xe3
#define _NRF24L01P_SPI_CMD_R_RX_PL_WID       0x60
#define _NRF24L01P_SPI_CMD_W_ACK_PAYLOAD     0xa8
#define _NRF24L01P_SPI_CMD_W_TX_PYLD_NO_ACK  0xb0
#define _NRF24L01P_SPI_CMD_NOP               0xff


#define _NRF24L01P_REG_CONFIG                0x00
#define _NRF24L01P_REG_EN_AA                 0x01
#define _NRF24L01P_REG_EN_RXADDR             0x02
#define _NRF24L01P_REG_SETUP_AW              0x03
#define _NRF24L01P_REG_SETUP_RETR            0x04
#define _NRF24L01P_REG_RF_CH                 0x05
#define _NRF24L01P_REG_RF_SETUP              0x06
#define _NRF24L01P_REG_STATUS                0x07
#define _NRF24L01P_REG_OBSERVE_TX            0x08
#define _NRF24L01P_REG_RPD                   0x09
#define _NRF24L01P_REG_RX_ADDR_P0            0x0a
#define _NRF24L01P_REG_RX_ADDR_P1            0x0b
#define _NRF24L01P_REG_RX_ADDR_P2            0x0c
#define _NRF24L01P_REG_RX_ADDR_P3            0x0d
#define _NRF24L01P_REG_RX_ADDR_P4            0x0e
#define _NRF24L01P_REG_RX_ADDR_P5            0x0f
#define _NRF24L01P_REG_TX_ADDR               0x10
#define _NRF24L01P_REG_RX_PW_P0              0x11
#define _NRF24L01P_REG_RX_PW_P1              0x12
#define _NRF24L01P_REG_RX_PW_P2              0x13
#define _NRF24L01P_REG_RX_PW_P3              0x14
#define _NRF24L01P_REG_RX_PW_P4              0x15
#define _NRF24L01P_REG_RX_PW_P5              0x16
#define _NRF24L01P_REG_FIFO_STATUS           0x17
#define _NRF24L01P_REG_DYNPD                 0x1c
#define _NRF24L01P_REG_FEATURE               0x1d

#define _NRF24L01P_REG_ADDRESS_MASK          0x1f

// CONFIG register:
#define _NRF24L01P_CONFIG_PRIM_RX        (1<<0)
#define _NRF24L01P_CONFIG_PWR_UP         (1<<1)
#define _NRF24L01P_CONFIG_CRC0           (1<<2)
#define _NRF24L01P_CONFIG_EN_CRC         (1<<3)
#define _NRF24L01P_CONFIG_MASK_MAX_RT    (1<<4)
#define _NRF24L01P_CONFIG_MASK_TX_DS     (1<<5)
#define _NRF24L01P_CONFIG_MASK_RX_DR     (1<<6)

#define _NRF24L01P_CONFIG_CRC_MASK       (_NRF24L01P_CONFIG_EN_CRC|_NRF24L01P_CONFIG_CRC0)
#define _NRF24L01P_CONFIG_CRC_NONE       (0)
#define _NRF24L01P_CONFIG_CRC_8BIT       (_NRF24L01P_CONFIG_EN_CRC)
#define _NRF24L01P_CONFIG_CRC_16BIT      (_NRF24L01P_CONFIG_EN_CRC|_NRF24L01P_CONFIG_CRC0)

// EN_AA register:
#define _NRF24L01P_EN_AA_NONE            0

// EN_RXADDR register:
#define _NRF24L01P_EN_RXADDR_NONE        0

// SETUP_AW register:
#define _NRF24L01P_SETUP_AW_AW_MASK      (0x3<<0)
#define _NRF24L01P_SETUP_AW_AW_3BYTE     (0x1<<0)
#define _NRF24L01P_SETUP_AW_AW_4BYTE     (0x2<<0)
#define _NRF24L01P_SETUP_AW_AW_5BYTE     (0x3<<0)

// SETUP_RETR register:
#define _NRF24L01P_SETUP_RETR_NONE       0

// RF_SETUP register:
#define _NRF24L01P_RF_SETUP_RF_PWR_MASK          (0x3<<1)
#define _NRF24L01P_RF_SETUP_RF_PWR_0DBM          (0x3<<1)
#define _NRF24L01P_RF_SETUP_RF_PWR_MINUS_6DBM    (0x2<<1)
#define _NRF24L01P_RF_SETUP_RF_PWR_MINUS_12DBM   (0x1<<1)
#define _NRF24L01P_RF_SETUP_RF_PWR_MINUS_18DBM   (0x0<<1)

#define _NRF24L01P_RF_SETUP_RF_DR_HIGH_BIT       (1 << 3)
#define _NRF24L01P_RF_SETUP_RF_DR_LOW_BIT        (1 << 5)
#define _NRF24L01P_RF_SETUP_RF_DR_MASK           (_NRF24L01P_RF_SETUP_RF_DR_LOW_BIT|_NRF24L01P_RF_SETUP_RF_DR_HIGH_BIT)
#define _NRF24L01P_RF_SETUP_RF_DR_250KBPS        (_NRF24L01P_RF_SETUP_RF_DR_LOW_BIT)
#define _NRF24L01P_RF_SETUP_RF_DR_1MBPS          (0)
#define _NRF24L01P_RF_SETUP_RF_DR_2MBPS          (_NRF24L01P_RF_SETUP_RF_DR_HIGH_BIT)

// STATUS register:
#define _NRF24L01P_STATUS_TX_FULL        (1<<0)
#define _NRF24L01P_STATUS_RX_P_NO        (0x7<<1)
#define _NRF24L01P_STATUS_MAX_RT         (1<<4)
#define _NRF24L01P_STATUS_TX_DS          (1<<5)
#define _NRF24L01P_STATUS_RX_DR          (1<<6)

// RX_PW_P0..RX_PW_P5 registers:
#define _NRF24L01P_RX_PW_Px_MASK         0x3F

#define _NRF24L01P_TIMING_Tundef2pd_us     100000   // 100mS
#define _NRF24L01P_TIMING_Tstby2a_us          130   // 130uS
#define _NRF24L01P_TIMING_Thce_us              10   //  10uS
#define _NRF24L01P_TIMING_Tpd2stby_us        4500   // 4.5mS worst case
#define _NRF24L01P_TIMING_Tpece2csn_us          4   //   4uS

typedef enum {
    _NRF24L01P_MODE_UNKNOWN,
    _NRF24L01P_MODE_POWER_DOWN,
    _NRF24L01P_MODE_STANDBY,
    _NRF24L01P_MODE_RX,
    _NRF24L01P_MODE_TX,
} nRF24L01P_Mode_Type;

SPI_HandleTypeDef SpiHandle;
extern uint32_t SpixTimeout;
static nRF24L01P_Mode_Type mode;
static uint8_t chipEnabled;
#define enableSlave HAL_GPIO_WritePin(GPIOB, GPIO_PIN_10, GPIO_PIN_RESET);
#define disableSlave HAL_GPIO_WritePin(GPIOB, GPIO_PIN_10, GPIO_PIN_SET);

static void init_SPI()
{
    SpiHandle.Instance = SPI1;
    SpiHandle.Init.Mode = SPI_MODE_MASTER;
    SpiHandle.Init.Direction = SPI_DIRECTION_2LINES;
    SpiHandle.Init.DataSize = SPI_DATASIZE_8BIT;
    SpiHandle.Init.CLKPolarity = SPI_POLARITY_LOW;
    SpiHandle.Init.CLKPhase = SPI_PHASE_1EDGE;
    SpiHandle.Init.NSS = SPI_NSS_SOFT; // for the time being keep it low
    SpiHandle.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_32;
    SpiHandle.Init.FirstBit = SPI_FIRSTBIT_MSB;
    SpiHandle.Init.TIMode = SPI_TIMODE_DISABLED;
    SpiHandle.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLED;
    SpiHandle.Init.CRCPolynomial = 7;
    /*
     *  Manage SPI CSN Pin by software
     */
    if(HAL_SPI_Init(&SpiHandle) != HAL_OK)
    {
        /* Initialization Error */
        Error_Handler();
    }
    disableSlave
}

static uint8_t SPIWriteRead(uint8_t Byte)
{
    uint8_t receivedbyte = 0;
    /* Send a Byte through the SPI peripheral */
    /* Read byte from the SPI bus */
    if(HAL_SPI_TransmitReceive(&SpiHandle, (uint8_t*) &Byte, (uint8_t*) &receivedbyte, 1, SpixTimeout) != HAL_OK)
    {
        Error_Handler();
    }
    return receivedbyte;
}
static int getRegister(int regAddress)
{
    int cn = (_NRF24L01P_SPI_CMD_RD_REG | (regAddress & _NRF24L01P_REG_ADDRESS_MASK));
    enableSlave
            SPIWriteRead(cn);
    int dn = SPIWriteRead(_NRF24L01P_SPI_CMD_NOP);
    disableSlave
            return dn;
}

void setRegister(int regAddress, int regData)
{
    int cn = (_NRF24L01P_SPI_CMD_WR_REG | (regAddress & _NRF24L01P_REG_ADDRESS_MASK));
    enableSlave
            SPIWriteRead(cn);
    SPIWriteRead(regData & 0xFF);
    disableSlave
            HAL_Delay(1); // 1ms delay

}

static void setAirDataRate(int rate)
{
    int rfSetup = getRegister(_NRF24L01P_REG_RF_SETUP) & ~_NRF24L01P_RF_SETUP_RF_DR_MASK;

    switch ( rate ) {

    case NRF24L01P_DATARATE_250_KBPS:
        rfSetup |= _NRF24L01P_RF_SETUP_RF_DR_250KBPS;
        break;

    case NRF24L01P_DATARATE_1_MBPS:
        rfSetup |= _NRF24L01P_RF_SETUP_RF_DR_1MBPS;
        break;

    case NRF24L01P_DATARATE_2_MBPS:
        rfSetup |= _NRF24L01P_RF_SETUP_RF_DR_2MBPS;
        break;

    default:
        Error_Handler();
        return;

    }

    setRegister(_NRF24L01P_REG_RF_SETUP, rfSetup);
}

static void setCrcWidth(int width)
{
    int config = getRegister(_NRF24L01P_REG_CONFIG) & ~_NRF24L01P_CONFIG_CRC_MASK;

    switch ( width )
    {
    case NRF24L01P_CRC_NONE:
        config |= _NRF24L01P_CONFIG_CRC_NONE;
        break;

    case NRF24L01P_CRC_8_BIT:
        config |= _NRF24L01P_CONFIG_CRC_8BIT;
        break;

    case NRF24L01P_CRC_16_BIT:
        config |= _NRF24L01P_CONFIG_CRC_16BIT;
        break;

    default:
        Error_Handler();
        return;
    }

    setRegister(_NRF24L01P_REG_CONFIG, config);

}

static void setTxAddress(unsigned long long address, int width)
{
    int setupAw = getRegister(_NRF24L01P_REG_SETUP_AW) & ~_NRF24L01P_SETUP_AW_AW_MASK;
    switch ( width )
    {
        case 3:
            setupAw |= _NRF24L01P_SETUP_AW_AW_3BYTE;
            break;

        case 4:
            setupAw |= _NRF24L01P_SETUP_AW_AW_4BYTE;
            break;

        case 5:
            setupAw |= _NRF24L01P_SETUP_AW_AW_5BYTE;
            break;

        default:
            Error_Handler();
            return;
    }

    setRegister(_NRF24L01P_REG_SETUP_AW, setupAw);
    int cn = (_NRF24L01P_SPI_CMD_WR_REG | (_NRF24L01P_REG_TX_ADDR & _NRF24L01P_REG_ADDRESS_MASK));
    enableSlave
    SPIWriteRead(cn);
    while ( width-- > 0 ) {

        //
        // LSByte first
        //
        SPIWriteRead((int) (address & 0xFF));
        address >>= 8;
    }
    disableSlave

}

static void setRxAddress(unsigned long long address, int width, int pipe)
{
    if ( ( pipe < NRF24L01P_PIPE_P0 ) || ( pipe > NRF24L01P_PIPE_P5 ) ) {

        Error_Handler();
        return;
    }

    if ( ( pipe == NRF24L01P_PIPE_P0 ) || ( pipe == NRF24L01P_PIPE_P1 ) )
    {

        int setupAw = getRegister(_NRF24L01P_REG_SETUP_AW) & ~_NRF24L01P_SETUP_AW_AW_MASK;

        switch ( width )
        {
        case 3:
            setupAw |= _NRF24L01P_SETUP_AW_AW_3BYTE;
            break;

        case 4:
            setupAw |= _NRF24L01P_SETUP_AW_AW_4BYTE;
            break;

        case 5:
            setupAw |= _NRF24L01P_SETUP_AW_AW_5BYTE;
            break;

        default:
            Error_Handler();
            return;

        }
        setRegister(_NRF24L01P_REG_SETUP_AW, setupAw);
    }
    else
        width = 1;

    int rxAddrPxRegister = _NRF24L01P_REG_RX_ADDR_P0 + ( pipe - NRF24L01P_PIPE_P0 );
    int cn = (_NRF24L01P_SPI_CMD_WR_REG | (rxAddrPxRegister & _NRF24L01P_REG_ADDRESS_MASK));
    enableSlave
    SPIWriteRead(cn);
    while ( width-- > 0 )
    {
        //
        // LSByte first
        //
        SPIWriteRead((int) (address & 0xFF));
        address >>= 8;
    }

    disableSlave
    int enRxAddr = getRegister(_NRF24L01P_REG_EN_RXADDR);
    enRxAddr |= (1 << ( pipe - NRF24L01P_PIPE_P0 ) );
    setRegister(_NRF24L01P_REG_EN_RXADDR, enRxAddr);
}

void initRfModule()
{
    init_SPI();
    HAL_Delay(100);

    //Power up
    mode = _NRF24L01P_MODE_UNKNOWN;
    int config = getRegister(_NRF24L01P_REG_CONFIG);
    config |= _NRF24L01P_CONFIG_PWR_UP;
    setRegister(_NRF24L01P_REG_CONFIG, config);
    HAL_Delay(5);//5 ms delay

    setRegister(_NRF24L01P_REG_STATUS, _NRF24L01P_STATUS_MAX_RT|
                _NRF24L01P_STATUS_TX_DS|_NRF24L01P_STATUS_RX_DR);   // Clear any pending interrupts
    setRegister(_NRF24L01P_REG_EN_RXADDR, _NRF24L01P_EN_RXADDR_NONE); //disable all rxpipes

    int channel = ( DEFAULT_NRF24L01P_RF_FREQUENCY - NRF24L01P_MIN_RF_FREQUENCY ) & 0x7F; //2.402 GHz
    setRegister(_NRF24L01P_REG_RF_CH, channel); // set the frequency

    setAirDataRate(DEFAULT_NRF24L01P_DATARATE); // 1 mbps default rate
    setCrcWidth(DEFAULT_NRF24L01P_CRC); // 8 bit crc
    setTxAddress(DEFAULT_NRF24L01P_ADDRESS, DEFAULT_NRF24L01P_ADDRESS_WIDTH); //set tx
    setRxAddress(DEFAULT_NRF24L01P_ADDRESS, DEFAULT_NRF24L01P_ADDRESS_WIDTH, NRF24L01P_PIPE_P0); // setrx
    setRegister(_NRF24L01P_REG_EN_AA, _NRF24L01P_EN_AA_NONE);//disable autoacknowledge
    setRegister(_NRF24L01P_REG_SETUP_RETR, _NRF24L01P_SETUP_RETR_NONE); //disable auto retransmit
    setRegister(_NRF24L01P_REG_RX_PW_P0, ( DEFAULT_NRF24L01P_TRANSFER_SIZE & _NRF24L01P_RX_PW_Px_MASK ) );//set transfer size
    mode = _NRF24L01P_MODE_STANDBY;
}


void enableChip(uint8_t enable)
{
    chipEnabled = enable;
    if(enable)
        HAL_GPIO_WritePin(GPIOA, GPIO_PIN_10, GPIO_PIN_SET);
    else
        HAL_GPIO_WritePin(GPIOA, GPIO_PIN_10, GPIO_PIN_RESET);
}

void setReceiveMode(void)
{
    int config = getRegister(_NRF24L01P_REG_CONFIG);
    config |= _NRF24L01P_CONFIG_PRIM_RX;
    setRegister(_NRF24L01P_REG_CONFIG, config);
    mode = _NRF24L01P_MODE_RX;
}

void setTransmitMode(void)
{
    int config = getRegister(_NRF24L01P_REG_CONFIG);
    config &= ~_NRF24L01P_CONFIG_PRIM_RX;
    setRegister(_NRF24L01P_REG_CONFIG, config);
    mode = _NRF24L01P_MODE_TX;
}


static int getStatusRegister(void)
{
    enableSlave
    int status = SPIWriteRead(_NRF24L01P_SPI_CMD_NOP);
    disableSlave
    return status;
}

int write(char *data, int count)
{
    int i = 0;
    int8_t originalCe = chipEnabled;
    enableChip(0);
    if ( count <= 0 )
        return 0;

    if ( count > _NRF24L01P_TX_FIFO_SIZE )
        count = _NRF24L01P_TX_FIFO_SIZE;

    // Clear the Status bit
    setRegister(_NRF24L01P_REG_STATUS, _NRF24L01P_STATUS_TX_DS); //clear interrupt
    enableSlave
    SPIWriteRead(_NRF24L01P_SPI_CMD_WR_TX_PAYLOAD);
    for ( i = 0; i < count; i++ )
        SPIWriteRead(*data++);

    disableSlave

    int originalMode = mode;
    setTransmitMode();
    enableChip(1);

    HAL_Delay(1); // 1 ms delay
    enableChip(0);

    while ( !( getStatusRegister() & _NRF24L01P_STATUS_TX_DS ) ) ;

    // Clear the Status bit
    setRegister(_NRF24L01P_REG_STATUS, _NRF24L01P_STATUS_TX_DS);
    if ( originalMode == _NRF24L01P_MODE_RX )
        setReceiveMode();

    enableChip(originalCe);
    HAL_Delay(1); // 1 ms delay
    return count;
}

uint8_t readable(uint8_t pipe)
{

    if ( ( pipe < NRF24L01P_PIPE_P0 ) || ( pipe > NRF24L01P_PIPE_P5 ) )
    {
        Error_Handler();
        return 0;
    }

    int status = getStatusRegister();

    return ( ( status & _NRF24L01P_STATUS_RX_DR ) && ( ( ( status & _NRF24L01P_STATUS_RX_P_NO ) >> 1 ) == ( pipe & 0x7 ) ) );

}

int read(int pipe, char *data, int count)
{
    if ( ( pipe < NRF24L01P_PIPE_P0 ) || ( pipe > NRF24L01P_PIPE_P5 ) )
    {
        Error_Handler();
        return -1;
    }

    if ( count <= 0 )
        return 0;

    if ( count > _NRF24L01P_RX_FIFO_SIZE )
        count = _NRF24L01P_RX_FIFO_SIZE;

    if ( readable(pipe) )
    {
        enableSlave
        SPIWriteRead(_NRF24L01P_SPI_CMD_R_RX_PL_WID);
        int rxPayloadWidth = SPIWriteRead(_NRF24L01P_SPI_CMD_NOP);
        disableSlave

        if ( ( rxPayloadWidth < 0 ) || ( rxPayloadWidth > _NRF24L01P_RX_FIFO_SIZE ) ) {

            // Received payload error: need to flush the FIFO

            enableSlave
            SPIWriteRead(_NRF24L01P_SPI_CMD_FLUSH_RX);
            SPIWriteRead(_NRF24L01P_SPI_CMD_NOP);
            disableSlave
        }
        else
        {
            if ( rxPayloadWidth < count ) count = rxPayloadWidth;
            enableSlave
            SPIWriteRead(_NRF24L01P_SPI_CMD_RD_RX_PAYLOAD);
            int i = 0;
            for ( i = 0; i < count; i++ )
                *data++ = SPIWriteRead(_NRF24L01P_SPI_CMD_NOP);

            disableSlave

            // Clear the Status bit
            setRegister(_NRF24L01P_REG_STATUS, _NRF24L01P_STATUS_RX_DR);
            return count;
        }
    }
    else
    {
        //
        // What should we do if there is no 'readable' data?
        //  We could wait for data to arrive, but for now, we'll
        //  just return with no data.
        //
        return 0;
    }
    //
    // We get here because an error condition occured;
    //  We could wait for data to arrive, but for now, we'll
    //  just return with no data.
    //
    return -1;
}
